from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_root():

    # Arrange
    # override any dependency injections

    # Act
    response = client.get("/")

    # Cleanup
    # normally reset overrides

    # Assert
    assert response.status_code == 200
    assert response.json() == {"Hello": "World"}
